#version 150
in vec2 inPosition;// input from the vertex buffer

uniform mat4 view;
uniform mat4 projection;

uniform int type;

const float PI = 3.1415;

vec3 getSphere(vec2 vec, vec3 add, vec3 mul){
    float az = vec.x * PI;
    float ze = vec.y * PI / 2;
    float r = 1;

    float x = r * cos(az) * cos(ze);
    float y = r * sin(az) * cos(ze);
    float z = r * sin(ze);
    return vec3(x, y, z) * mul + add;
}

void main() {
    vec2 pos = inPosition * 2 - 1;
    //vec4 pos4 = vec4(position, getZ(position), 1.0);
    vec4 pos4;

    vec3 add = vec3(0, 0, 3);
    vec3 mul = vec3(2, 1, 0.5);
    pos4 = vec4(getSphere(pos, add, mul), 1.0);

    gl_Position = projection * view * pos4;
} 
