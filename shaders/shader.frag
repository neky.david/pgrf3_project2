#version 150

/*in vec3 vertColor;*/

out vec4 outColor;// output from the fragment shader
in vec3 normal;
in vec3 lightDirection;
in vec3 viewDirection;
in vec4 depthTextureCoord;
in vec2 texCoord;
in vec3 position;

in float lightDistance;

uniform vec3 lightPosition;
uniform sampler2D deptTexture;

uniform int objectIndex;
uniform vec2 clickPos;
uniform bool renderToScreen;
uniform sampler2D objectTexture;

void main() {
    /*outColor = vec4(vertColor, 1.0);*/

    vec4 phong;
    vec4 ambient;

    //útlum
    float att = 1.0/(0.1 * lightDistance);

    //AMBIENT
    ambient = vec4(vec3(0.2), 1);

    vec3 ld = normalize(lightDirection);
    vec3 nd = normalize(normal);

    //DIFFUSE
    float NdotL = max(0.0, dot(nd, ld));
    vec4 diffuse = vec4(NdotL * vec3(0.45), 1);

    //SPECULAR - zrcadlova
    vec3 halfVector = normalize(ld + normalize(viewDirection));
    float NdotH = dot(normalize(normal), halfVector);
    vec4 specular = vec4(pow(NdotH, 16) * vec3(0.8), 1);

    phong = ambient + att * (diffuse + specular);

    float zL = texture(deptTexture, depthTextureCoord.xy).r;//rgb - každá složka je stejná.. gl_FragCoord.zzz
    float zA = depthTextureCoord.z;

    //lze vyzkouset ruzne hodnoty 0.1 0.01 0.001..
    bool shadow = zL < zA - 0.001;
    vec4 finalColor;

    if (shadow){
        finalColor = ambient;
    } else {
        finalColor = phong;
    }

    int y = int((1 - gl_FragCoord.y / 768) * 768);

    if (renderToScreen){
        /*if ((gl_FragCoord.x < clickPos.x + 5 && gl_FragCoord.x > clickPos.x - 5) &&
        (y < clickPos.y + 5 && y > clickPos.y - 5)){
            outColor = finalColor * vec4(1.0, 0.0, 0.0, 1.0);
        }*/
        //vec2 cp = vec2(clickPos.x / 1024, clickPos.y / 768);

        float clickObjectIndex = texture(objectTexture, clickPos).g;
        if (clickObjectIndex == objectIndex){
            outColor = finalColor * vec4(1.0, 0.0, 0.0, 1.0);
        }
        else {
            outColor = finalColor * vec4(1.0, 1.0, 1.0, 1.0);
        }
        //outColor = vec4(texture(objectTexture, gl_FragCoord.xy));

        //outColor = vec4(objectIndex)
    }
    else {
        outColor = vec4(objectIndex);
    }


    //outColor = depthTextureCoord;
    //outColor = vec4(normalize(normal), 1.0);
} 
