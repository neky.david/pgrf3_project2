#version 150
in vec2 inPosition;// input from the vertex buffer

uniform mat4 view;
uniform mat4 projection;
uniform mat4 lightViewProjection;
uniform vec3 lightPosition;

uniform int objectIndex;
uniform sampler2D objectTexture;

out vec3 normal;
out vec3 lightDirection;
out vec3 viewDirection;
out vec4 depthTextureCoord;
out vec2 texCoord;
out vec3 position;

out float lightDistance;

const float PI = 3.1415;


vec3 getSphere(vec2 vec){
    float az = vec.x * PI;
    float ze = vec.y * PI / 2;
    float r = 1.2;

    float x = r * cos(az) * cos(ze);
    float y = r * sin(az) * cos(ze);
    float z = r * sin(ze);

    if(mod(objectIndex, 4) > 1){
        if(mod(objectIndex, 2) > 0){
            return vec3(x, y, z) + vec3(-objectIndex/2 - 0.5, 0, -objectIndex/2 - 0.5);
        }
        else{
            return vec3(x, y, z) + vec3(objectIndex/2 + 0.5, 0, -objectIndex/2 - 0.5);
        }
    }else{
        if(mod(objectIndex, 2) > 0){
            return vec3(x, y, z) + vec3(-objectIndex/2 - 1.5, 0, objectIndex/2 + 1.5);
        }
        else{
            return vec3(x, y, z) + vec3(objectIndex/2 + 1.5, 0, objectIndex/2 + 1.5);
        }
    }
}
vec3 getSphereNormal(vec2 vec){
    vec3 u = getSphere(vec + vec2(0.001, 0)) - getSphere(vec - vec2(0.001, 0));
    vec3 v = getSphere(vec + vec2(0, 0.001)) - getSphere(vec - vec2(0, 0.001));
    return cross(u, v);
}

void main() {
    vec2 pos = inPosition * 2 - 1;
    //vec4 pos4 = vec4(position, getZ(position), 1.0);
    vec4 pos4;

    pos4 = vec4(getSphere(pos), 1.0);
    normal = mat3(view) * getSphereNormal(pos);

    position = pos4.xyz;
    gl_Position = projection * view * pos4;

    /*vertColor = pos4.xyz;*/

    //vec3 lightPos = vec3(1,1,0);
    lightDirection = lightPosition - pos4.xyz;

    lightDistance = length(lightDirection);

    viewDirection = - (view * pos4).xyz;

    texCoord = inPosition;

    depthTextureCoord = lightViewProjection * pos4;
    depthTextureCoord.xyz = depthTextureCoord.xyz / depthTextureCoord.w;
    depthTextureCoord.xyz = (depthTextureCoord.xyz + 1) / 2;

}
