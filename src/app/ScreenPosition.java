package app;

public class ScreenPosition {

    private float x;
    private float y;

    public ScreenPosition(){}

    public ScreenPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public ScreenPosition normalize(int width, int height){
        ScreenPosition sp = new ScreenPosition();
        sp.setX(x/width);
        sp.setY(Math.abs((y/height)-1));
        return sp;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() == ScreenPosition.class){
            return ((ScreenPosition)obj).getX() == x && ((ScreenPosition)obj).getY() == y;
        }
        return false;
    }

    @Override
    public String toString() {
        return "x: " + x + ", y: " + y;
    }
}
