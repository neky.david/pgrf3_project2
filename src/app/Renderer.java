package app;

import lwjglutils.*;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.*;
import transforms.*;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.EXTFramebufferObject.glBindFramebufferEXT;
import static org.lwjgl.opengl.GL11.GL_FILL;
import static org.lwjgl.opengl.GL11C.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11C.GL_FRONT_AND_BACK;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengles.GLES20.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengles.GLES20.GL_DEPTH_TEST;


/**
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2019-09-02
 */
public class Renderer extends AbstractRenderer {

    private final int OBJECT_COUNT = 16;

    private int shaderProgram, shaderProgramLight;
    private OGLBuffers buffers;
    private int locView, locProjection, locLightVP;
    private int locViewLight, locProjectionLight, locLightPosition;
    private int locObjectIndex, locClickPos, locRenderToScreen;

    private int frameBufferId;

    private Camera camera, cameraLight;
    private Mat4PerspRH projPersp;
    private Vec3D lightPosition;
    private ScreenPosition clickPosition;

    // The window handle
    private boolean mouseButton1 = false;
    private double ox, oy;
    private ScreenPosition pressPos = new ScreenPosition(-1, -1);

    private double zenith = 9 / 7f * Math.PI;

    private OGLRenderTarget lightRenderTarget;
    private OGLRenderTarget objectsRenderTarget;

    @Override
    public void init() {
        //glClearColor(0.3f, 0.3f, 0.3f, 1f);
        glEnable(GL_DEPTH_TEST);

        //projekt
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //GL_FRONT_AND_BACK, GL_FRONT... GL_FILL, GL_LINE, GL_POINT

        frameBufferId = glGenFramebuffers();
        glBindFramebufferEXT(GL_FRAMEBUFFER, frameBufferId);

        shaderProgram = ShaderUtils.loadProgram("/shader");
        shaderProgramLight = ShaderUtils.loadProgram("/light");

        locView = glGetUniformLocation(shaderProgram, "view");
        locProjection = glGetUniformLocation(shaderProgram, "projection");
        locLightVP = glGetUniformLocation(shaderProgram, "lightViewProjection");
        locLightPosition = glGetUniformLocation(shaderProgram, "lightPosition");

        locObjectIndex = glGetUniformLocation(shaderProgram, "objectIndex");
        locClickPos = glGetUniformLocation(shaderProgram, "clickPos");
        locRenderToScreen = glGetUniformLocation(shaderProgram, "renderToScreen");

        locViewLight = glGetUniformLocation(shaderProgramLight, "view");
        locProjectionLight = glGetUniformLocation(shaderProgramLight, "projection");
        buffers = GridFactory.generateGrid(100, 100);

        lightRenderTarget = new OGLRenderTarget(width, height);
        objectsRenderTarget = new OGLRenderTarget(width, height);

        textRenderer = new OGLTextRenderer(1024, 50);

        camera = new Camera()
                .withPosition(new Vec3D(0, 12, 0))
                .withAzimuth(4.6) //0 - rovne.. PI - 180°.. 2*PI - rovne
                .withZenith(0.1) // PI/2 nahoru; -PI/2 dolu
                .withFirstPerson(false)
                .withRadius(5);

        projPersp = new Mat4PerspRH(
                Math.PI / 3,
                LwjglWindow.HEIGHT / (float) LwjglWindow.WIDTH,
                1, 60);

        textRenderer.resize(width, height);

        clickPosition = new ScreenPosition(width/2, height/2);
    }

    @Override
    public void display() {

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //GL_FRONT_AND_BACK, GL_FRONT... GL_FILL, GL_LINE, GL_POINT

        renderFromLight();
        renderFromViewer(false);
        renderFromViewer(true);

        drawPlus();
    }

    private void renderFromLight() {
        glUseProgram(shaderProgramLight);
        lightRenderTarget.bind();

        glClearColor(0, 0.5f, 0.3f, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        lightPosition = new Vec3D(3, 10, 10);

        cameraLight = new Camera()
                .withPosition(lightPosition)
                .withAzimuth(Math.PI / 2)//0 - rovne.. PI - 180°.. 2*PI - rovne
                .withZenith(zenith);// PI/2 nahoru; -PI/2 dolu ... -1 / 5f * Math.PI

        glUniformMatrix4fv(locViewLight, false, cameraLight.getViewMatrix().floatArray());
        glUniformMatrix4fv(locProjectionLight, false, projPersp.floatArray());

        drawBuffers();
    }

    private void renderFromViewer(boolean renderToScreen) {
        //Nastavení shader programu
        glUseProgram(shaderProgram);
        //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if(renderToScreen){
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
        }
        else {
            objectsRenderTarget.bind();
        }

        glUniform1i(locRenderToScreen, renderToScreen ? 1 : 0);

        //Nastavení ViewPortu na obrazovku
        glViewport(0, 0, width, height);

        glClearColor(0.1f, 0.1f, 0.1f, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //glUniformMatrix4fv(locView, false, view);
        glUniformMatrix4fv(locView, false, camera.getViewMatrix().floatArray());
        glUniformMatrix4fv(locProjection, false, projPersp.floatArray());
        glUniformMatrix4fv(locLightVP, false, cameraLight.getViewMatrix().mul(projPersp).floatArray()); //viewLight.mul(projection).floatArray());

        glUniform3f(locLightPosition, (float) lightPosition.getX(), (float) lightPosition.getY(), (float) lightPosition.getZ());

        ScreenPosition normClickPos = clickPosition.normalize(width, height);
        System.out.println(clickPosition.toString());
        glUniform2f(locClickPos, normClickPos.getX(), normClickPos.getY());

        //Bindování textury na CPU
        lightRenderTarget.getDepthTexture().bind(shaderProgram, "deptTexture", 1);
        //lightRenderTarget.getColorTexture().bind(shaderProgram, "deptTexture", 1);

        if(renderToScreen){
            objectsRenderTarget.getColorTexture().bind(shaderProgram, "objectTexture", 2);
        }

        drawBuffers();
    }

    private void drawBuffers() {
        for (int i = 0; i < OBJECT_COUNT; i++) {
            glUniform1i(locObjectIndex, i);
            buffers.draw(GL_TRIANGLES, shaderProgram);
        }
    }

    private void drawPlus() {
        String text = "+";

        //create and draw text
        textRenderer.clear();

        int x = (int)clickPosition.getX()-8;
        int y = (int)clickPosition.getY()+8;

        if(x < 10) x = 10;
        if(y < 30) y = 30;

        textRenderer.addStr2D(x, y, text);
        textRenderer.draw();
        glEnable(GL_DEPTH_TEST);
    }

    private GLFWKeyCallback keyCallback = new GLFWKeyCallback() {
        @Override
        public void invoke(long window, int key, int scancode, int action, int mods) {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
            if (action == GLFW_PRESS || action == GLFW_REPEAT) {
                switch (key) {
                    case GLFW_KEY_W:
                        camera = camera.forward(1);
                        break;
                    case GLFW_KEY_D:
                        camera = camera.right(1);
                        break;
                    case GLFW_KEY_S:
                        camera = camera.backward(1);
                        break;
                    case GLFW_KEY_A:
                        camera = camera.left(1);
                        break;
                    case GLFW_KEY_LEFT_CONTROL:
                        camera = camera.down(1);
                        break;
                    case GLFW_KEY_LEFT_SHIFT:
                        camera = camera.up(1);
                        break;
                    case GLFW_KEY_SPACE:
                        camera = camera.withFirstPerson(!camera.getFirstPerson());
                        break;
                    case GLFW_KEY_R:
                        camera = camera.mulRadius(0.9f);
                        break;
                    case GLFW_KEY_F:
                        camera = camera.mulRadius(1.1f);
                        break;
                }
            }
        }
    };

    private GLFWWindowSizeCallback wsCallback = new GLFWWindowSizeCallback() {
        @Override
        public void invoke(long window, int w, int h) {
            if (w > 0 && h > 0) {
                width = w;
                height = h;
                System.out.println("Windows resize to [" + w + ", " + h + "]");
                if (textRenderer != null)
                    textRenderer.resize(width, height);
            }

            projPersp = new Mat4PerspRH(Math.PI / 3, h / (float) w, 1, 60);
        }
    };

    private GLFWMouseButtonCallback mbCallback = new GLFWMouseButtonCallback() {

        @Override
        public void invoke(long window, int button, int action, int mods) {
            mouseButton1 = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS;

            if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {

                mouseButton1 = true;
                DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                glfwGetCursorPos(window, xBuffer, yBuffer);
                ox = xBuffer.get(0);
                oy = yBuffer.get(0);

                pressPos = new ScreenPosition((int)ox , (int)oy);
            }

            if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE) {
                mouseButton1 = false;
                DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                glfwGetCursorPos(window, xBuffer, yBuffer);
                double x = xBuffer.get(0);
                double y = yBuffer.get(0);
                camera = camera.addAzimuth((double) Math.PI * (ox - x) / width)
                        .addZenith((double) Math.PI * (oy - y) / width);
                ox = x;
                oy = y;

                if(pressPos.getX() == x && pressPos.getY() == y){
                    clickPosition = new ScreenPosition((int) ox, (int) oy);
                }
            }
        }
    };

    private GLFWCursorPosCallback cpCallbacknew = new GLFWCursorPosCallback() {
        @Override
        public void invoke(long window, double x, double y) {
            if (mouseButton1) {
                camera = camera.addAzimuth((double) Math.PI * (ox - x) / width)
                        .addZenith((double) Math.PI * (oy - y) / width);
                ox = x;
                oy = y;
            }
        }
    };

    @Override
    public GLFWCursorPosCallback getCursorCallback() {
        return cpCallbacknew;
    }

    @Override
    public GLFWKeyCallback getKeyCallback() {
        return keyCallback;
    }

    @Override
    public GLFWWindowSizeCallback getWsCallback() {
        return wsCallback;
    }

    @Override
    public GLFWMouseButtonCallback getMouseCallback() {
        return mbCallback;
    }

    @Override
    public GLFWScrollCallback getScrollCallback() {
        return scrollCallback;
    }
}